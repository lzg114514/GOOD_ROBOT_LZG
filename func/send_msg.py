import requests

def send_md_msg(title, message, webhook_url):
    '''
    userid: @用户 钉钉id
    title : 消息标题
    message: 消息主体内容
    webhook_url: 通讯url
    '''
    data = {
        "msgtype": "markdown",
        "markdown": {
            "title": title,
            "text": message
        }
    }
    # 利用requests发送post请求
    req = requests.post(webhook_url, json=data)


def send_text_msg(content, webhook_url):
    '''
    userid: @用户 钉钉id
    title : 消息标题
    message: 消息主体内容
    webhook_url: 通讯url
    '''
    data = {
        "msgtype": "text",
        "text": {"content": content},
    }
    # 利用requests发送post请求
    #print(data)
    req = requests.post(webhook_url, json=data)

def send_link_msg(title, text, messageURL, webhook_url):
    data = {
        "msgtype" : "link",
        "link" : {
            "text" : text,
            "title" : title,
            "messageUrl" : messageURL,
            "picUrl" : ""
        }
    }
    req = requests.post(webhook_url, json=data)

def send_actionCard_msg(title: str, text: str, btns: list, webhook_url):
    data = {
        "msgtype" : "actionCard",
        "actionCard": {
            "title" : title,
            "text" : text,
            "btnOrientation" : "0",
            "btns" : btns
        }
    }
    req = requests.post(webhook_url, json=data)
    #print(req.text)

def getActionCardBtnData(btn_title, actionURL):
    data = {
        "title" : btn_title,
        "actionURL" : actionURL
    }
    return data

def send_file_msg(media_id: str,fileType: str, file_name: str, webhook_url: str):
    data = {
        "msgtype" : "file",
        "file" : {
            "mediaId" : media_id,
            "fileType" : fileType,
            "fileName" : file_name
        }
    }

    req = requests.post(webhook_url, json=data)